<?php
error_reporting(E_ALL | E_STRICT);

/**
 * Reorders an array in order to display it in a column-wise layout
 *
 * @param  array $array     Array to transform into column layout
 * @param  int   $columns   Number of columns to create
 * @param  bool  $addHeader Generate descriptive header data for each column
 * @return array An array consisting either of the body data or of header and
 *               body data
 */
function transformIntoColumns(array $array, $columns, $addHeader = true)
{
    // Sanity checking
    if ($columns < 1) {
        return ($addHeader) ? array(array(), array()) : array();
    }

    // Return values
    $header = array();
    $body   = array();

    // Derived values
    $elements   = count($array);
    $rows       = ceil($elements / $columns);
    $filledCols = ($elements % $columns != 0) ? $elements % $columns : $columns;

    // Calculate header content
    if ($addHeader) {
        for ($c = 0; $c < $columns; ++$c) {
            $index1 = $c * $rows - max($c - $filledCols, 0);
            $index2 = ($c < $filledCols) ? $index1 + $rows - 1
                                         : $index1 + $rows - 2;
            $index2 = min($index2, $elements - 1);

            $letter1 = ($index1 < $elements) ? mb_substr($array[$index1], 0, 1)
                                             : '';
            $letter2 = mb_substr($array[$index2], 0, 1);

            if ($letter1 == '') {
                $header[] = '';
            } else {
                $header[] = ($letter1 == $letter2) ? $letter1
                                                   : $letter1 . '-' . $letter2;
            }
        }
    }

    // Calculate body content
    for ($r = 0; $r < $rows; ++$r) {
        $tmp = array();

        for ($c = 0; $c < $columns; ++$c) {
            if ($r + 1 == $rows && $c >= $filledCols) {
                $index = $elements;
            } else {
                $index = $r + $c * $rows - max($c - $filledCols, 0);
            }

            $tmp[] = ($index < $elements) ? $array[$index] : '';
        }

        $body[] = $tmp;
    }

    if ($addHeader) {
        return array($header, $body);
    }

    return $body;
}

list($header, $body) = transformIntoColumns(range('A', 'Z'), 3);

?>

<table style="width: 100%;">
<thead>
<tr>
<?php
$cnt = count($header);
for ($i = 0; $i < $cnt; $i++) :
    $style  = ($i + 1 == $cnt) ? ''
                               : ' style="width: ' . round(100 / $cnt) . '%";';
?>
    <th<?php echo $style; ?>><?php
        echo ($header[$i] != '') ? $header[$i] : 'n/a';
    ?></th>
<?php
endfor;
?>
</tr>
</thead>
<tbody>
<?php
foreach ($body as $row) :
?>
<tr>
<?php
    foreach ($row as $cell) :
?>
    <td><?php echo ($cell != '') ? $cell : '&nbsp;'; ?></td>
<?php
    endforeach;
?>
</tr>
<?php
endforeach;
?>
</tbody>
</table>
