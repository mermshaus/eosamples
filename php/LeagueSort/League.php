<?php

class League
{
    protected $_matches = array();
    protected $_teams   = array();

    public function addMatch(Match $match)
    {
        $this->_matches[] = $match;
        return $this;
    }

    public function createTeam($teamname)
    {
        $team = new Team($teamname);
        $this->_teams[] = $team;

        return $team;
    }

    protected function _sortTeams(Team $a, Team $b)
    {
        // Absolute number of points
        if ($a->points != $b->points) {
            return ($a->points < $b->points);
        }

        // Direct comparison and away goals

        // Goals first team (x=a)
        $xgoals = 0;

        // Goals second team (y=b)
        $ygoals = 0;

        // Away goals team a
        $xagoals = 0;

        // Away goals team b
        $yagoals = 0;

        foreach ($this->_matches as $match) {
            if ($match->hteam === $a && $match->ateam === $b) {
                $xgoals += $match->hgoals;
                $ygoals += $match->agoals;
                $yagoals = $match->agoals;
            } else if ($match->hteam === $b && $match->ateam === $a) {
                $ygoals += $match->hgoals;
                $xgoals += $match->agoals;
                $xagoals = $match->agoals;
            }
        }

        if ($xgoals != $ygoals) {
            return ($xgoals < $ygoals);
        }
        if ($xagoals != $yagoals) {
            return ($xagoals < $yagoals);
        }

        // Goal difference
        $adiff = ($a->hgoals + $a->agoals) - ($a->hgoalsAgainst + $a->agoalsAgainst);
        $bdiff = ($b->hgoals + $b->agoals) - ($b->hgoalsAgainst + $b->agoalsAgainst);

        if ($adiff != $bdiff) {
            return ($adiff < $bdiff);
        }

        // Absolute number of goals scored during season
        if (($a->hgoals + $a->agoals) != ($b->hgoals + $b->agoals)) {
            return (($a->hgoals + $a->agoals) < ($b->hgoals + $b->agoals));
        }

        // Team order undefined (possibly same rank)
        //throw new Exception('Couldn\'t determine team order.');
    }

    public function getTable()
    {
        foreach ($this->_matches as $match) {
            if ($match->hgoals > $match->agoals) {
                $match->hteam->points += 3;

                $match->hteam->wins++;
                $match->ateam->losses++;
            } else if ($match->hgoals == $match->agoals) {
                $match->hteam->points += 1;
                $match->ateam->points += 1;

                $match->hteam->draws++;
                $match->ateam->draws++;
            } else {
                $match->ateam->points += 3;

                $match->hteam->losses++;
                $match->ateam->wins++;
            }

            $match->hteam->hgoals += $match->hgoals;
            $match->hteam->hgoalsAgainst += $match->agoals;

            $match->ateam->agoals += $match->agoals;
            $match->ateam->agoalsAgainst += $match->hgoals;

            $match->hteam->gamesPlayed++;
            $match->ateam->gamesPlayed++;
        }

        $teamsSorted = $this->_teams;

        usort($teamsSorted, array($this, '_sortTeams'));

        return $teamsSorted;
    }
}
