<?php

require_once './League.php';
require_once './Match.php';
require_once './Team.php';

$league = new League();

$teams = array(
    0 => $league->createTeam('Manchester City'),
    1 => $league->createTeam('Chelsea'),
    2 => $league->createTeam('Arsenal'),
    3 => $league->createTeam('Blackburn Rovers')
);

$league->addMatch(new Match($teams[0], $teams[1], 2, 0))
       ->addMatch(new Match($teams[0], $teams[2], 2, 1))
       ->addMatch(new Match($teams[0], $teams[3], 2, 0))

       ->addMatch(new Match($teams[1], $teams[0], 2, 0))
       ->addMatch(new Match($teams[1], $teams[2], 3, 5))
       ->addMatch(new Match($teams[1], $teams[3], 2, 0))

       ->addMatch(new Match($teams[2], $teams[0], 2, 0))
       ->addMatch(new Match($teams[2], $teams[1], 3, 0))
       ->addMatch(new Match($teams[2], $teams[3], 2, 0))

       ->addMatch(new Match($teams[3], $teams[0], 2, 0))
       ->addMatch(new Match($teams[3], $teams[1], 1, 1))
       ->addMatch(new Match($teams[3], $teams[2], 2, 0));

$table = $league->getTable();


echo '<table border="1">';

echo '<tr>';
echo '<td>Rank</td>';
echo '<td>Team</td>';
echo '<td>Games</td>';
echo '<td>W</td>';
echo '<td>D</td>';
echo '<td>L</td>';
echo '<td>Goals</td>';
echo '<td>Goals Diff</td>';
echo '<td>Points</td>';

echo '</tr>';
$i = 1;
foreach ($table as $team) {
    echo '<tr>';
    echo '<td>' . $i++ . '</td>';
    echo '<td>' . $team->teamname . '</td>';
    echo '<td>' . $team->gamesPlayed . '</td>';
    echo '<td>' . $team->wins . '</td>';
    echo '<td>' . $team->draws . '</td>';
    echo '<td>' . $team->losses . '</td>';
    echo '<td>' . ($team->hgoals + $team->agoals) . ':' . ($team->hgoalsAgainst + $team->agoalsAgainst) . '</td>';
    echo '<td>' . (($team->hgoals + $team->agoals) - ($team->hgoalsAgainst + $team->agoalsAgainst)) . '</td>';
    echo '<td>' . $team->points . '</td>';
    echo '</tr>';
}

echo '</table>';
