<?php

/**
 *
 */
class Team
{
    public $teamname;
    public $agoals = 0;
    public $hgoals = 0;
    public $agoalsAgainst = 0;
    public $hgoalsAgainst = 0;

    public $gamesPlayed = 0;

    public $wins = 0;
    public $draws = 0;
    public $losses = 0;

    public $points = 0;

    public function __construct($teamname)
    {
        $this->teamname     = $teamname;
    }
}
