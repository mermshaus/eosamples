<?php

/**
 *
 */
class Match
{
    /**
     * Home team
     *
     * @var Team
     */
    public $hteam;

    /**
     * Away team
     *
     * @var Team
     */
    public $ateam;

    /**
     * Home team goals
     *
     * @var int
     */
    public $hgoals;

    /**
     * Away team goals
     *
     * @var int
     */
    public $agoals;

    public function __construct(Team $hteam, Team $ateam, $hgoals, $agoals)
    {
        $this->hteam = $hteam;
        $this->ateam = $ateam;
        $this->hgoals = $hgoals;
        $this->agoals = $agoals;
    }
}
